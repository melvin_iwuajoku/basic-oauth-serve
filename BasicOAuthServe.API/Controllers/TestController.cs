﻿using System.Security.Claims;
using System.Web.Http;

namespace BasicOAuthServe.Server.Controllers
{
    public class TestController : ApiController
    {
        // GET api/<controller>
        [Route("test")]
        public IHttpActionResult Get()
        {
            var caller = User as ClaimsPrincipal;

            var subjectClaim = caller.FindFirst("sub");
            if (subjectClaim != null)
            {
                return Json(new
                {
                    message = "Ok user",
                    client = caller.FindFirst("client_id").Value,
                    sucject = subjectClaim.Value
                });
            }
            else
            {
                return Json(new
                {
                    message = "OK Computer",
                    client = caller.FindFirst("client_id").Value
                });                                              
            }
        }
    }
}