﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace BasicOAuthServe.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var tokenResopnse = GetClientToken();
            CallApi(tokenResopnse);
            GetUserToken();

            Console.ReadLine();
        }

        static TokenResponse GetClientToken()
        {
            var client = new TokenClient("http://localhost:5000/connect/token", "silicon", "F621F470-9731-4A25-80EF-67A6F7C5F4B8");
            

            return client.RequestClientCredentialsAsync("api1").Result;
        }

        static void CallApi(TokenResponse response)
        {
            var client = new HttpClient();
            client.SetBearerToken(response.AccessToken);

            Console.WriteLine(client.GetStringAsync("http://localhost:40319/test").Result);
        }

        private static TokenResponse GetUserToken()
        {
            var client = new TokenClient("http://localhost:5000/connect/token", "carbon", "AA0F6153-A182-4F52-B96C-D4595CAB7C62");

            return client.RequestResourceOwnerPasswordAsync("Bob", "secret", "api1").Result;
        }
    }
}
