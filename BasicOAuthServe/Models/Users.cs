using System.Collections.Generic;
using IdentityServer3.Core.Services.InMemory;

namespace BasicOAuthServe.Models
{
    static class Users
    {
        public static List<InMemoryUser> Get()
        {
            return new List<InMemoryUser>
            {
                new InMemoryUser()
                {
                    Username = "Bob",
                    Password = "secret",
                    Subject = "1"
                },
                new InMemoryUser()
                {
                    Username = "Alice",
                    Password = "secret",
                    Subject = "2"
                }
            };
        }
    }
}